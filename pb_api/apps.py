from django.apps import AppConfig


class PbApiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'pb_api'
