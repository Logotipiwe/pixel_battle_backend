from django.db import models


class Field(models.Model):
    title = models.CharField(max_length=255)
    height = models.IntegerField()
    width = models.IntegerField()
