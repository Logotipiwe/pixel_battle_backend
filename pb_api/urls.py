from django.urls import path, include

urlpatterns = [
    path('pb/api', include('pb_api.urls')),
]
